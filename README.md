# Software Studio 2021 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Chatroom

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

# 作品網址：
https://mid-chatroom-eebf5.web.app

## Website Detail Description

![web_img](web_img.png)
![signin_img](signin_img.png)

        這是一個有登入頁面和聊天室頁面的Chatroom，使用者可以透過註冊、或直接用google登入，進入聊天室。進入聊天室後，大家一開始都可以看見global chatroom的內容，也可以在裡面發文；使用者也可以選擇創建新的private chatroom，並加入想邀請的好友。
        在chatroom內，可以發送文字訊息（包含html code），以及上傳圖片、影片、語音檔案。

![img1](img1.png)

        每則訊息都包含發送人頭像、發送人email、訊息內容、發送時間。其中發送人頭像如果是使用者本人，就會是黃色；若是別人，就會是紅色。

![img2](img2.png)

        每次有使用者在內的聊天室有新訊息，都會跳訊息通知出來；當使用者被加入新聊天室時，也會有通知跳出來。

# Components Description : 
1. Membership Mechanism  : 使用者可以在signin頁面註冊，或用現有的帳號登入。
2. Host on your Firebase page : 利用firebase deploy聊天室頁面，連結在上方「作品網址」欄。
3. Database read/write  : 訊息的儲存與載入是利用firebase的realtime database，每次進入網頁都加載一次，當使用者發送訊息時也會將data push到database中。
4. RWD : 當螢幕縮小時，每個element會隨著縮小的螢幕變動。
5. Topic Key Functions : 使用者可以自己創建新的私人chatroom，並加入好友；被加入的人可以在自己的頁面看到新加入的聊天室。
6. Sign Up/In with Google or other third-party accounts : 使用者可以透過google第三方網頁登入。
7. Add Chrome notification : 當使用者所在的聊天室有新訊息、或被加進新聊天室時，會跳出Chrome通知（監聽database的變動，當儲存訊息的database有變動時，跳出通知）。
8. Use CSS animation : 在登入頁面以及聊天室名稱區域有做顏色改變CSS動畫。
9. Deal with messages when sending html code : 使用者可以傳送html code不會出錯（利用字串部分替換，可以將'<'換成'&lt\;'、'>'換成'&gt\;'）。

# Other Functions Description : 
1. Upload image : 使用者可以上傳圖片至chatroom中。利用firebase的storage功能，先將使用者上傳的圖片push到storage中，並取得storage的url，再表現在聊天室中。
2. Upload video : 使用者可以上傳影片至chatroom中。利用firebase的storage功能，先將使用者上傳的影片push到storage中，並取得storage的url，再表現在聊天室中。
3. Upload audio : 使用者可以上傳audio至chatroom中。利用firebase的storage功能，先將使用者上傳的audio push到storage中，並取得storage的url，再表現在聊天室中。
4. Message sent time : 利用Javascript的new Date()函式，取得使用者發送訊息的時間，再利用toLocaleString()轉成叫精簡的表現方式，最後連同訊息內容一起push到database中。
5. 使用者頭像色塊 : 判斷此訊息的發送人是否為目前登入的使用者，若是，則將頭像色塊設為黃色；若否，則設為紅色。

