var cur_room = 0;
var room_count = 0;
function listen_to_new_message(id){
    var Ref = firebase.database().ref('com_list/'+id);
    var user = firebase.auth().currentUser;
    var flag = 0;
    Ref.endAt().limitToLast(1).on('child_added', function(snapshot){
        var data = snapshot.val();
        //console.log(data);
        if(!flag){
            flag = 1;
        }else{
            if(data.email != user.email){
                if(Notification.permission === 'granted'){
                    var notification = new Notification("new message from "+data.email+": "+data.data);
                }
            }
        }
    });
}
function init_notif(){
    firebase.auth().onAuthStateChanged(user => {
        if(user){
            var path = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('usr_room_list/'+path);
            listen_to_new_message(0);
            Ref.once('value').then((snapshot)=>{
                snapshot.forEach((childshot)=>{
                    var childData = childshot.val();
                    listen_to_new_message(childData[0]);
                });
            });
            var flag = 0;
            Ref.endAt().limitToLast(1).on('child_added', function(snapshot) {
                var name = snapshot.val();
                if(!flag){
                    flag = 1;
                }else{
                    if(Notification.permission === 'granted'){
                        var notification = new Notification("You have been added to room '"+name[1]+"'");
                    }
                    listen_to_new_message(name[0]);
                } 
            });
        }
    });
}




function init_img(){
    img_btn = document.getElementById('img_btn');

    img_btn.addEventListener('change', function(e){
        var usr = firebase.auth().currentUser;
        var file = e.target.files[0];
        //console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(()=>{
            storageRef.getDownloadURL().then((url)=>{
                var t = new Date();
                var data = {
                    email: usr.email,
                    type: 1,
                    data: '',
                    url: url,
                    date: t.toLocaleString()
                }
                var postsRef = firebase.database().ref('com_list/'+cur_room);
                postsRef.push(data);
            });
        });
    });
}
function init_video(){
    video_btn = document.getElementById('video_btn');
    video_btn.addEventListener('change', function(e){
        var usr = firebase.auth().currentUser;
        var file = e.target.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(()=>{
            storageRef.getDownloadURL().then((url)=>{
                var t = new Date();
                var data = {
                    email: usr.email,
                    type: 2,
                    data: '',
                    url: url,
                    date: t.toLocaleString()
                }
                var postsRef = firebase.database().ref('com_list/'+cur_room);
                postsRef.push(data);
            });
        });
    });
}

function init_audio(){
    audio_btn = document.getElementById('audio_btn');
    //console.log(hi);
    audio_btn.addEventListener('change', function(e){
        var usr = firebase.auth().currentUser;
        var file = e.target.files[0];
        console.log(file);
        var storageRef = firebase.storage().ref(file.name);
        storageRef.put(file).then(()=>{
            storageRef.getDownloadURL().then((url)=>{
                var t = new Date();
                var data = {
                    email: usr.email,
                    type: 3,
                    data: '',
                    url: url,
                    date: t.toLocaleString()
                }
                var postsRef = firebase.database().ref('com_list/'+cur_room);
                postsRef.push(data);
            });
        });
    });
}

function init_room_list(){
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            var path = user.email.replace(/\./g, ',');
            var Ref = firebase.database().ref('usr_room_list/'+path);
            var first_count = 0;
            var second_count = 0;
            //console.log('hey');
            Ref.once('value').then((snapshot)=>{
                snapshot.forEach((childshot)=>{
                    var childData = childshot.val();
                    //console.log(childData);
                    var html_code = "<a class='dropdown-item' onclick='change_room("+childData[0]+")'>"+childData[1]+"</a>";
                    document.getElementById('bar').innerHTML += html_code;
                    first_count++;
                });
                Ref.on('child_added', (data)=>{
                    second_count++;
                    if (second_count > first_count) {
                        var childData = data.val();
                        var html_code = "<a class='dropdown-item' onclick='change_room("+childData[0]+")'>"+childData[1]+"</a>";
                        document.getElementById('bar').innerHTML += html_code;
                    }
                });
            });
        }else{
            document.getElementById('chatroom_name').innerHTML = "Global Chatroom";
            document.getElementById('sub_title').innerHTML = "Let's Chat!";
        }
    });
    
   
 
}

function init_usr(){
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        
        if (user) {
            
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        
            document.getElementById('logout-btn').addEventListener('click', function(){
                firebase.auth().signOut().then(()=>{
                    alert('log out success!');
                }).catch(()=>{
                    alert('log out failed!');
                });
            });
        } else {
            
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            document.getElementById('room_sel_bar').style.display = "none";
        }
    });
}

function post_handle(e){
    if(e.keyCode == 13){
        post_txt = document.getElementById('comment');
        if (post_txt.value != "") {
                
            var Ref = firebase.database().ref('com_list/'+cur_room);
            var usr = firebase.auth().currentUser;
            if(usr){
                var t = new Date();
                var data = {
                    data: post_txt.value,
                    email: usr.email,
                    type: 0,
                    url: '',
                    date: t.toLocaleString(),
                };
                Ref.push(data);
                
            }
            post_txt.value = "";
        }
        post_txt.value = "";
    }
}

function init() {

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if (post_txt.value != "") {
            var txt = post_txt.value.replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
            console.log(txt);
            var Ref = firebase.database().ref('com_list/'+cur_room);
            var usr = firebase.auth().currentUser;
            if(usr){
                var t = new Date();
                console.log(t);
                var data = {
                    data: txt,
                    email: usr.email,
                    type: 0,
                    url: '',
                    date: t.toLocaleString(),
                };
                console.log(t.toLocaleString());
                Ref.push(data);
                var note = {
                    body: usr.email+': \n'+txt,
                }
                if(Notification.permission === 'granted'){
                    var notification = new Notification('From Chatroom.', note); 
                }
            }
            post_txt.value = "";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow' style='width: 100%;'><div class='media text-muted pt-3'><div class='rounded' style='height: 32px; width: 32px; background-color: #DB465C; margin-right: 15px'></div><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    var str_before_username_self = "<div class='my-3 p-3 bg-white rounded box-shadow' style='width: 100%;'><div class='media text-muted pt-3'><div class='rounded' style='height: 32px; width: 32px; background-color: #FAB624; margin-right: 15px'></div><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";
    var str_before_v = "<video height='300px' controls><source src='";
    var str_after_v = "' type='video/mp4'></video>";
    var str_before_a = "<audio controls><source src='";
    var str_after_a = "' type='audio/mpeg'></audio>";

    var postsRef = firebase.database().ref('com_list/'+cur_room);
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;


    postsRef.once('value')
        .then(function(snapshot) {
            
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var usr = firebase.auth().currentUser;
                if(childData.type === 0){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 1){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 2){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 3){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                }
                //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var usr = firebase.auth().currentUser;
                    if(childData.type === 0){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 1){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 2){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 3){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                    }
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });


        })
        .catch(e => console.log(e.message));
}

function listen_to_newRoom(){
    var first_count = 0;
    var second_count = 0;
    var Ref_2 = firebase.database().ref('all_room');
    var Ref = firebase.database().ref('com_list/'+cur_room);
    var total_post = [];
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow' style='width: 100%;'><div class='media text-muted pt-3'><div class='rounded' style='height: 32px; width: 32px; background-color: #DB465C; margin-right: 15px'></div><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";
    var str_before_username_self = "<div class='my-3 p-3 bg-white rounded box-shadow' style='width: 100%;'><div class='media text-muted pt-3'><div class='rounded' style='height: 32px; width: 32px; background-color: #FAB624; margin-right: 15px'></div><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";
    var str_before_v = "<video height='300px' controls><source src='";
    var str_after_v = "' type='video/mp4'></video>";
    var str_before_a = "<audio controls><source src='";
    var str_after_a = "' type='audio/mpeg'></audio>";



    if(!cur_room){
        document.getElementById('chatroom_name').innerHTML = "Global Chatroom";
        document.getElementById('sub_title').innerHTML = "Let's Chat!";
    }else{
        Ref_2.once('value').then((snapshot)=>{
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.room_id == cur_room){
                    document.getElementById('chatroom_name').innerHTML = childData.room_name;
                    document.getElementById('sub_title').innerHTML = "private chatroom!";
                }
            });
        });
    }

    Ref.once('value')
        .then(function(snapshot) {

            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                var usr = firebase.auth().currentUser;
                if(childData.type === 0){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 1){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 2){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                }else if(childData.type === 3){
                    if(usr.email == childData.email){
                        total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                    }
                    else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                }
                //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                first_count += 1;
            });


            document.getElementById('post_list').innerHTML = total_post.join('');

            Ref.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    var usr = firebase.auth().currentUser;
                    if(childData.type === 0){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 1){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 2){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_v + childData.url + str_after_v + '<br/><br/>' + childData.date +str_after_content;
                    }else if(childData.type === 3){
                        if(usr.email == childData.email){
                            total_post[total_post.length] = str_before_username_self + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                        }
                        else total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_a + childData.url + str_after_a + '<br/><br/>' + childData.date +str_after_content;
                    }
                    //total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });


        });
}
function open_popup(){
    var usr = firebase.auth().currentUser;
    if(usr){
        document.getElementById("popup").style.display = "block";
    }
}
function close_popup(){
    document.getElementById("popup").style.display = "none";
}

function add_ppl(){
    var usr = firebase.auth().currentUser;
    
    if(usr && cur_room){
        var path = document.getElementById("popup_input").value.replace(/\./g, ',');
        var Ref = firebase.database().ref('usr_room_list/'+path);
        var Ref_2 = firebase.database().ref('all_room');
        var room_n = '';
        if(path){
            Ref_2.once('value').then((snapshot)=>{
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.room_id == cur_room){
                        room_n = childData.room_name;
                    }
                });
                var data = [cur_room, room_n];
                Ref.push(data);
                alert('successfully add '+document.getElementById("popup_input").value+' to the room!');
                document.getElementById("popup_input").value = "";
            });
        }
        close_popup();
    }
    //close_popup();
}

function change_room(room){
    firebase.database().ref('com_list/'+cur_room).off('child_added');
    cur_room = room;
    document.getElementById('post_list').innerHTML = "";
    listen_to_newRoom();
}

function add_room(e){
    
    if (e.keyCode == 13){
        var input_field = document.getElementById('input_field');
        var Ref = firebase.database().ref('all_room');
        var usr = firebase.auth().currentUser;
        room_count = 0;
        Ref.once('value').then((snapshot)=>{
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                room_count = childData.room_id;
                console.log(room_count);
            });
            room_count++;
            var data = {
                room_name: input_field.value,
                room_id: room_count,
            };
            Ref.push(data);
            var path = usr.email.replace(/\./g, ',');
            var Ref_2 = firebase.database().ref('usr_room_list/'+path);
            var data_2 = [room_count, input_field.value];
            Ref_2.push(data_2);
            room_count = 0;
        });
    }
}
window.onload = function() {
    Notification.requestPermission();
    console.log(Notification.permission);
    firebase.auth().onAuthStateChanged(()=>{
        init_notif();
        init_room_list();
        init_usr();
        init();
        init_img();
        init_video();
        init_audio();
    });
};
